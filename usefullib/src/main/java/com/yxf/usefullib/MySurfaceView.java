package com.yxf.usefullib;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback, Handler.Callback {

    public static final String TAG = "MySurfaceView";

    public static final int MESSAGE_DRAW = 0;

    private boolean isQuitHandlerThreadWhenDestroy = true;

    private HandlerThread handlerThread;
    private WeakHandler handler;


    public MySurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getHolder().addCallback(this);
    }

    public MySurfaceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MySurfaceView(Context context) {
        this(context, null);
    }

    public void refresh() {
        if (handler == null) {
            return;
        }
        Message message = Message.obtain();
        message.what = MESSAGE_DRAW;
        handler.removeMessages(MESSAGE_DRAW);
        handler.sendMessage(message);
    }

    public WeakHandler getThreadHandler() {
        return handler;
    }

    public WeakHandler setHandlerThread(HandlerThread thread) {
        return setHandlerThread(thread, null);
    }

    protected WeakHandler setHandlerThread(HandlerThread thread, Handler.Callback callback) {
        if (thread == null) {
            Log.w(TAG, "the HandlerThread set is null");
            return null;
        }
        return initHandler(thread, callback, null);
    }

    private WeakHandler initHandler(HandlerThread thread, Handler.Callback callback, WeakHandler h) {
        this.handlerThread = thread;
        if (handlerThread.getLooper() == null) {
            handlerThread.start();
        }
        if (callback == null) {
            callback = this;
        }
        if (h == null) {
            handler = new WeakHandler(thread.getLooper(), callback);
        } else {
            handler = h;
        }
        return handler;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (handlerThread == null) {
            handlerThread = new HandlerThread(TAG);
            initHandler(handlerThread, null, null);
            isQuitHandlerThreadWhenDestroy = true;
        }
        refresh();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        handler.removeMessages(MESSAGE_DRAW);
        if (isQuitHandlerThreadWhenDestroy) {
            handlerThread.quitSafely();
            handlerThread = null;
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_DRAW:
                Canvas canvas = getHolder().lockCanvas();
                if (canvas != null) {
                    drawFrame(canvas);
                    getHolder().unlockCanvasAndPost(canvas);
                }
                return true;
        }
        return false;
    }

    public void drawFrame(Canvas canvas) {

    }
}